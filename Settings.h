#pragma once

#include <SFML/Graphics.hpp>

/**
 * Settings class holds all settings existing in the configuration file. To add
 * a new setting: 
 * 1) Add a new member variable
 * 2) Add the variable to the initialisation list
 * 3) Write a parse function for it in SettingsReader class
 */
class Settings
{
public:
    Settings() : resolution(800, 600),
                 vsyc(false),
                 title("Default title"),
                 frameLimit(-1) {}
    ~Settings(){}
    
    sf::Vector2i resolution;
    bool vsyc;
    std::string title;
    int frameLimit;
};
