#include "Bullet.h"
#include "AssetManager.h"
#include <iostream>

const float Bullet::BULLET_SPEED = 14.0f;
const float Bullet::SPIN_SPEED = 4.4f;

/**
 * A Bullet
 * 
 * @param pos_ Starting position of the bullet
 * @param vel_ Velocity vector
 * @param level_ Level where bullet is fired
 * @param bullets_ Bullets container
 */
Bullet::Bullet(sf::Vector2f* pos_,
               sf::Vector2f* vel_,
               Level* level_) :
    BaseObject(pos_, vel_, SpriteAsset::BULLET), level(level_),  spinAngle(0.0f)
{   
    vel->x *= BULLET_SPEED;
    vel->y *= BULLET_SPEED;    
}

/**
 * Updates the bullet
 */
void Bullet::Update()
{
    
    spinAngle += SPIN_SPEED;

    // Just in case of overflow, cap at 360
    if (spinAngle > 360.0f)
    {
        spinAngle -= 360.0f;
    }
        
    sprite->setRotation(spinAngle);

	BaseObject::Update();
}
