#ifndef _PLAY_STATE_H_
#define	_PLAY_STATE_H_

#include "types.h"
#include "IState.h"
#include "Player.h"
#include "BaseObject.h"
#include "Camera.h"
#include "Level.h"
#include "Enemy.h"
#include "ObjectManager.h"
#include <vector>

/*
Ok, so this class is going to handle gameplay. The way I have design this, that means this will handle a lot of game logic.
For instance, it will handle a player object, a collection of enemies to iterate and update and a collection of bullets
to iterate and update and check for collision against the enemies. It would be nice to have a more abstracted way to do this
but I'm not 1337 enough to really know a better way. So I'll do it like this for better or worse and it will be a learning
experience!
*/

class PlayState : public IState
{
public:
	PlayState(Game* game);
	~PlayState(void);

	virtual void DoInit();
	virtual void ReInit();
	// Input will be checked in Update so I don't think HandleEvents needs to do anything
	virtual void HandleEvents(sf::Event event);
	// Updates all the components of the state and handles logic
	virtual void Update(void);
	// Checks for input and responds
	virtual void HandleInput(void);
	// Draws everything in this state
	virtual void Draw();        

protected:
    virtual void Cleanup();

    // PlayState specific stuff
    void Shoot();

    Player* player;
    int screenWidth;
    int screenHeight;

private:
    static void UpdateObject(BaseObject* baseObject, void* params);
    static void DrawObject(BaseObject* baseObject, void* params);
    static void HandleCollisions(BaseObject* baseObject, void* params);
    Camera* camera;
    Level level;
    bool debugOut;
    sf::Font debugFont;
    sf::Text debugText;  
    sf::Clock shootTimer;
    sf::Font livesFont;
    sf::Text livesText;  
    sf::Texture texBullet;
    ObjectManager objectManager;
    Enemy* enemy;
};

#endif /* _PLAY_STATE_H_ */
