#pragma once

#include <SFML/Graphics.hpp>

/**
 * This class represents a level of the game. It contains data like level size
 * and shape as well as any kind of logic
 * 
 * @TODO Probably this will be a base class, overriden by more specific
 * level classes
 */
class Level
{
public:
    
    Level();
    ~Level();
    
    void Draw(sf::RenderWindow& window);    
    
    // Maximum view sizes of the level. Since the shape could vary, this is 
    // required for the camera to know the render limits. 
    // @TODO At moment, view limits and level limits are the same. This might
    // not be the case in the future.
    const static int MAX_WIDTH;
    const static int MAX_HEIGHT;
    
    // @TODO A test variable to render the levels borders. I guess this will not
    // be needed, sprites will be used instead.
    const static int THICKNESS;
        
private:
    sf::ConvexShape border; // Border of the level
    sf::Texture* background; // Background texture
};
