INCDIRS=-I/home/manji/various/libs/SFML-2.0-rc/include
LIBDIRS=-L/home/manji/various/libs/SFML-2.0-rc/lib
LIBS= -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio -lsfml-network
FLAGS= -Wall -std=c++0x

geom: main.o Game.o StateManager.o SettingsReader.o PlayState.o AssetManager.o BaseObject.o Bullet.o Camera.o Level.o Player.o Enemy.o ObjectManager.o Util.o Collision.o
	g++ main.o Game.o StateManager.o SettingsReader.o PlayState.o AssetManager.o BaseObject.o Bullet.o Camera.o Level.o Player.o Enemy.o ObjectManager.o Util.o Collision.o -o geom $(LIBDIRS) $(LIBS) 

main.o: main.cpp Game.h
	g++ -c $(INCDIRS) $(FLAGS) main.cpp

Game.o: Game.cpp Game.h PlayState.h Settings.h SettingsReader.h
	g++ -c $(INCDIRS) $(FLAGS) Game.cpp

StateManager.o: StateManager.cpp StateManager.h IState.h
	g++ -c $(INCDIRS) $(FLAGS) StateManager.cpp 

SettingsReader.o: SettingsReader.cpp SettingsReader.h
	g++ -c $(INCDIRS) $(FLAGS) SettingsReader.cpp

PlayState.o: PlayState.cpp PlayState.h
	g++ -c $(INCDIRS) $(FLAGS) PlayState.cpp

AssetManager.o: AssetManager.cpp AssetManager.h
	g++ -c $(INCDIRS) $(FLAGS) AssetManager.cpp
	
BaseObject.o: BaseObject.cpp BaseObject.h
	g++ -c $(INCDIRS) $(FLAGS)  BaseObject.cpp

Bullet.o: Bullet.cpp Bullet.h
	g++ -c $(INCDIRS) $(FLAGS)  Bullet.cpp

Camera.o: Camera.cpp Camera.h
	g++ -c $(INCDIRS) $(FLAGS)  Camera.cpp

Level.o: Level.cpp Level.h
	g++ -c $(INCDIRS) $(FLAGS)  Level.cpp

Player.o: Player.cpp Player.h
	g++ -c $(INCDIRS) $(FLAGS)  Player.cpp

Enemy.o: Enemy.cpp Enemy.h
	g++ -c $(INCDIRS) $(FLAGS)  Enemy.cpp

ObjectManager.o: ObjectManager.cpp ObjectManager.h
	g++ -c $(INCDIRS) $(FLAGS)  ObjectManager.cpp

Collision.o: Collision.cpp Collision.h
	g++ -c $(INCDIRS) $(FLAGS)  Collision.cpp

Util.o: Util.cpp Util.h
	g++ -c $(INCDIRS) $(FLAGS)  Util.cpp

clean:
	rm -f *.o geom

run:
	./geom


#
#
#
