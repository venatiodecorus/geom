#include "PlayState.h"
#include "Enemy.h"
#include "ObjectManager.h"
#include "Bullet.h"
#include <iostream>
#include <math.h>
#include <cstdlib>
#include <stdio.h>
#include <sstream>

using namespace std;

PlayState::PlayState(Game* game) : 
		IState(game)
{
    screenWidth = 0;
    screenHeight = 0;

    screenWidth = game->window.getSize().x;
    screenHeight = game->window.getSize().y;

    debugFont.loadFromFile("graphics/arial.ttf");
    debugText.setFont(debugFont);
    debugText.setString("Test text");
    debugText.setColor(sf::Color::Red);
    debugText.setPosition(300,300);
    
    texBullet.loadFromFile("graphics/bullet.png");

    debugOut = false;
}

PlayState::~PlayState(void)
{
}

void PlayState::DoInit()
{
	// @TODO SM - Position and velocity will have to change to pointers, so you
	// can call Insert with one line
    enemy = new Enemy(new sf::Vector2f(300.0f, 300.0f),
								   new sf::Vector2f(0.2f, 0.2f),
								   &level);
	objectManager.Insert(enemy);
	objectManager.Insert(new Enemy(new sf::Vector2f(100.0f, 400.0f),
								   new sf::Vector2f(-1.0f, 3.0f),
								   &level));

	player = new Player(game, &level,
						new sf::Vector2f (300.0f, 300.0f),
					    new sf::Vector2f (0.0f, 0.0f));
	objectManager.Insert(player);

	// Camera need to be instantiated after player
	camera = new Camera(&game->window, level, player);
}

void PlayState::ReInit()
{
}

void PlayState::HandleEvents(sf::Event event)
{
}

void PlayState::HandleInput()
{
    // TODO Possibly make these class vars?
    sf::Keyboard k;
    sf::Mouse m;

    if(k.isKeyPressed(k.B))
    {
        debugOut = !debugOut;
    }

    // Going to handle all input here now
    // Accelerate the player
    // TODO Might want to add a maximum here
    if(k.isKeyPressed(k.Up) || k.isKeyPressed(k.W))
    {
            player->GetVel()->y -= player->GetSpeed();
    }
    if(k.isKeyPressed(k.Down) || k.isKeyPressed(k.S))
    {
            player->GetVel()->y += player->GetSpeed();
    }
    if(k.isKeyPressed(k.Left) || k.isKeyPressed(k.A))
    {
            player->GetVel()->x -= player->GetSpeed();
    }
    if(k.isKeyPressed(k.Right) || k.isKeyPressed(k.D))
    {
            player->GetVel()->x += player->GetSpeed();
    }

    // Shoot on pressed and check for the time
    if(m.isButtonPressed(m.Left) && 
       shootTimer.getElapsedTime().asMilliseconds() > player->SHOOT_DELAY)
    {
        Shoot();
        // @TODO Do we really need extra timers for these things? Can't we use
        // the game's timer (if any) and check the difference between each key 
        // pressed? (same with polarity)

        // RAL Yeah I think we could. I just figured this would be easier when I did it but
        // it would be more efficient to use the gameTimer.
        shootTimer.restart();
    }
}

void PlayState::Update()
{
	HandleInput();

	camera->Update();
    
	objectManager.ForEach(&UpdateObject);

	debugText.setPosition(camera->GetWindowVector() + sf::Vector2f(-600,300));
    
    objectManager.ForEach(&HandleCollisions, player);
}

void PlayState::Draw()
{
    camera->Move();

    level.Draw(game->window);

    objectManager.ForEach(&DrawObject, &(game->window));

    if(debugOut)
    {
        std::ostringstream debugString; 
        debugString << "Player Pos: " << player->GetPos()->x << ", " << player->GetPos()->y << " Player Speed: " << player->GetSpeed() << " Player Vel: " << player->GetVel()->x << ", " << player->GetVel()->y;
        debugText.setString(debugString.str());
        game->window.draw(debugText);
    }
}

void PlayState::Cleanup()
{
    delete camera;
}

// Called when the player hits the shoot button, creates a bullet based on the mouse location and adds
// it to the bullet list
void PlayState::Shoot()
{
    sf::Vector2i mousePos;
    sf::Vector2f mouseWorldPos;
    sf::Vector2f playerPos;
    sf::Vector2f dir;
    
    mousePos = sf::Mouse::getPosition(game->window);
    playerPos.x = player->GetSprite()->getGlobalBounds().left;
    playerPos.y = player->GetSprite()->getGlobalBounds().top;

    //Center player pos in the middle of the ship, otherwise shooting isn't accurate
    playerPos.x += player->GetTexture()->getSize().x/2;
    playerPos.y += player->GetTexture()->getSize().y/2;

    mouseWorldPos = game->window.convertCoords(mousePos);

    dir = mouseWorldPos - playerPos;

    float dlen = sqrt((float)(dir.x*dir.x + dir.y*dir.y));
    dir.x /= dlen;
    dir.y /= dlen;

    objectManager.Insert(new Bullet(new sf::Vector2f(*player->GetPos()),
                                                     new sf::Vector2f(dir),
                                                     &level));
}

/**
 * Update a base object
 */
void PlayState::UpdateObject(BaseObject* baseObject, void* params)
{
	baseObject->Update();
}

/**
 * Handle collisions. At the moment, it only checks for collisions between
 * the player and rest of the objects.
 */
void PlayState::HandleCollisions(BaseObject* baseObject, void* params)
{
    Player* player = (Player*)params;
    // TODO: Here we exclude the player from the checks, so it does not collide
    // with itself. Later, player will probably be not part of the hash.
    if (baseObject->GetId() != player->GetId() && 
        player->Collides(baseObject))
    {
        // Collision occurred! Handle response here.
    }
}

void PlayState::DrawObject(BaseObject* baseObject, void* params)
{
    sf::RenderWindow* window = (sf::RenderWindow*)params;
    baseObject->Draw(*window);
}
