#ifndef _BASE_OBJECT_H_
#define _BASE_OBJECT_H_

#include <SFML/Graphics.hpp>
#include "AssetManager.h"
#include "Collision.h"

#ifndef PI
    #define PI (3.14159265358979323846)
#endif
#define RADIANS_PER_DEGREE (PI/180.0)

// TODO *** Definitely need some kind of gameTime for the updates, I'm getting crazy movement

// This is the base object for all drawable objects. I might want to think about adding a flag to
// enable or disable the object incase I get problems with certain inputs..
class BaseObject : Collision
{
public:
    BaseObject() {}
    BaseObject(sf::Vector2f* pos_, sf::Vector2f* vel_, SpriteAsset sprite);
    ~BaseObject();
    virtual void Update();	// Probably need to pass this gameTime of some sort eventually
    virtual void Draw(sf::RenderWindow& window);
    float GetSpeed() { return speed; }
    sf::Vector2f* GetVel() { return vel; }
    sf::Vector2f* GetPos() { return pos; }
    sf::Sprite* GetSprite() { return sprite; }
    sf::Texture* GetTexture() { return texture; }
    unsigned int GetId() { return id; }
    bool Collides(BaseObject* obj);

protected:
    sf::Vector2f* pos;
    sf::Vector2f* vel;
    float speed;
    float drag;
    sf::Sprite* sprite;
    sf::Texture* texture; // Sprite only holds a reference, so we need to keep the texture here
    unsigned int id;

private:
    bool BoundingBoxTest(const sf::Sprite& Object1, const sf::Sprite& Object2);
};

#endif /* _BASE_OBJECT_H_ */
