#pragma once

#include "Settings.h"

class SettingsReader
{
public:
    SettingsReader(){}
    ~SettingsReader(){}
    bool Read(const char* fileName, Settings& settings);

private:
    std::string trim(std::string const& source, char const* delims = " \t\r\n");
    void Read(Settings& settings,
              const std::string& name,
              const std::string& value);
    
    void ParseResolution(Settings& settings,
                         const std::string& name,
                         const std::string& value);
    void ParseVSync(Settings& settings,
                    const std::string& name,
                    const std::string& value);
    void ParseTitle(Settings& settings,
                    const std::string& name,
                    const std::string& value);
    void ParseFrameLimit(Settings& settings,
                         const std::string& name,
                         const std::string& value);
};