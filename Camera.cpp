#include "Camera.h"
#include "Util.h"
#include "Level.h"
#include <algorithm>
#include <iostream>

/**
 * This class represents a camera, the (physical) player's view of the game. It
 * follows the (game's) player without exceeding certain limits
 * 
 * @param window_ The window of the game
 * @param level The current level of the game
 * @param player_ The player of the game
 */
Camera::Camera(sf::RenderWindow* window_, const Level& level, Player* player_):
        window(window_),
        player(player_),
        cameraView((sf::View*)&window->getDefaultView()),
        cameraPosition(cameraView->getCenter()),
        windowWidth(window->getSize().x),
        windowHeight(window->getSize().y),
        isStaticX(false),
        isStaticY(false)
{
    xDiff = std::abs(windowWidth - level.MAX_WIDTH);
    yDiff = std::abs(level.MAX_HEIGHT - windowHeight);        

    // If the length of a dimension of the level is less than the window's,
    // we will not bother with the camera update on this axis. We place the 
    // camera in the middle.
    if (level.MAX_WIDTH < window->getSize().x)
    {
        cameraPosition.x = level.MAX_WIDTH/2;
        isStaticX = true;
    }
    if (level.MAX_HEIGHT < window->getSize().y)
    {
        cameraPosition.y = level.MAX_HEIGHT/2;
        isStaticY = true;
    }
}

/**
 * Updates the camera's position if required.
 */
void Camera::Update()
{
    // We do not bother updating if camera is not supposed to move. But if
    // we do bother, here is what is going on. SFML defines the view's 
    // position as its center. We always follow the player, except for the 
    // cases where whn doing so, the view will cover areas it should not.
    if (!isStaticX)
    {            
        cameraPosition.x = Util::Clamp(player->GetPos()->x,
                                       windowWidth/2,
                                       windowWidth/2 + xDiff);
    }
    if (!isStaticY)
    {
        cameraPosition.y = Util::Clamp(player->GetPos()->y,
                                       windowHeight/2,
                                       windowHeight/2 + yDiff);
    }
}

/**
 * Moves the camera based on its position updated in Update()
 */
void Camera::Move()
{
    cameraView->setCenter(cameraPosition);
    window->setView(*cameraView);
}


/*
 * Gets the camera position
 */
sf::Vector2f Camera::GetWindowVector()
{
    return cameraPosition;
}



