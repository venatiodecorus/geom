#pragma once

#include "Player.h"
#include "Level.h"

class Camera
{
public:
    Camera(sf::RenderWindow* window_, const Level& level, Player* player_);
    ~Camera(){};
    void Move();        
    void Update();
    sf::Vector2f GetWindowVector();
        
private:
    sf::RenderWindow* window;
    sf::View* cameraView;
    sf::Vector2f cameraPosition;        
    Player* player;

    // Difference between window sizes and level sizes
    float xDiff;
    float yDiff;

    // Holders of window size
    int windowWidth;
    int windowHeight;

    // Boolean flags showing whether the camera should move or not
    bool isStaticX;
    bool isStaticY;
};
