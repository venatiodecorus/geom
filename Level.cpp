#include "Level.h"

const int Level::THICKNESS = 20;
const int Level::MAX_WIDTH = 2000 - THICKNESS;
const int Level::MAX_HEIGHT = 1000 - THICKNESS;

Level::Level():
        background(new sf::Texture)
{    
    background->loadFromFile("graphics/stars.png");

    // @TODO This does not seem to work
    background->setRepeated(true);

    border.setPointCount(4);
    border.setTexture((const sf::Texture*)background, false);        
    border.setPoint(0, sf::Vector2f(THICKNESS, THICKNESS));
    border.setPoint(1, sf::Vector2f(MAX_WIDTH - THICKNESS, THICKNESS));
    border.setPoint(2, sf::Vector2f(MAX_WIDTH - THICKNESS,
                                    MAX_HEIGHT - THICKNESS));
    border.setPoint(3, sf::Vector2f(THICKNESS, MAX_HEIGHT - THICKNESS));
    border.setOutlineThickness(THICKNESS);
}

Level::~Level()
{
    delete background;
}

/**
 * Draws the level
 */
void Level::Draw(sf::RenderWindow& window)
{  
    window.draw(border);      
}