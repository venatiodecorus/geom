#pragma once

#include "BaseObject.h"
#include "Level.h"
#include <iostream>

class Bullet : public BaseObject
{
public:
    Bullet(sf::Vector2f* pos_,
           sf::Vector2f* vel_,
           Level* level_);
    ~Bullet(){};
    void Update();
    
private:        
    Level* level;  
    float spinAngle;
    
    const static float BULLET_SPEED; // Duration of inverted polarity
    const static float SPIN_SPEED; // Duration of inverted polarity
};
