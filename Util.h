#ifndef UTIL_H
#define	UTIL_H

/**
 * This class contains static functions to be used by any class
 */
class Util
{    
public:    
    static float Clamp (float currentValue, float minValue, float maxValue);  
    static unsigned int GetNextId ();
    
private:
    static unsigned id;
};

#endif	/* UTIL_H */