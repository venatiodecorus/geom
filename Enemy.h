#pragma once

#include "BaseObject.h"
#include "Level.h"
#include "Game.h"
#include <SFML/Graphics.hpp>

class Enemy : public BaseObject
{
public: 
    Enemy(sf::Vector2f* pos_,
          sf::Vector2f* vel_,
          Level* level_);
    ~Enemy(void){}
    void Update();

private:    
    Level* level;
    static const unsigned int ENEMY_SPEED = 5.0f;
};
