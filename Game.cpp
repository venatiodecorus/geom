#include "Game.h"
#include "PlayState.h"
#include "Settings.h"
#include "SettingsReader.h"
#include "AssetManager.h"
#include <iostream>

Game::Game(void)
{
	UPDATE_RATE = 5; //ms
	updateTime = 0;
}


Game::~Game(void)
{
}

void Game::Init()
{    
    Settings settings;    
    SettingsReader settingsReader;    
    const char* cfgFile = "settings.cfg";
    if (!settingsReader.Read(cfgFile, settings))
    {
        std::cout << "Error reading configuration file: \"" << cfgFile 
                  << "\". Using default configuration." << std::endl;
    }
    
    // Create the main window    
    window.create(sf::VideoMode(settings.resolution.x, settings.resolution.y),
                  settings.title);    

    window.setVerticalSyncEnabled(settings.vsyc);
    
    if (settings.frameLimit > 0)
    {
        window.setFramerateLimit(settings.frameLimit);
    }
    
    AssetManager assetManager;
    assetManager.Initialise();
    
    // Was using PlayState p(this) to create a new object and then passed &p to stateMan.AddState()
    // That was WRONG. Definitely needs to be an IState base object and use the new operator to make
    // it a PlayState. Intresting.
    IState* p = new PlayState(this);
    stateMan.AddState(p);
}

void Game::Run()
{
	IState* state;// = stateMan.GetActiveState();
	//state->Draw();

	 // Start the game loop
     while (window.isOpen())
     {
		 state = stateMan.GetActiveState();

         // Process events
         sf::Event event;
         while (window.pollEvent(event))
         {
             // Close window : exit
             if (event.type == sf::Event::Closed)
                 window.close();

			 state->HandleEvents(event);
         }

		 // Update the update accumulator
		 updateTime += timer.getElapsedTime().asMilliseconds();

		 // Only update the state according to our update rate. Everything else should be fine going as fast
		 // as it can, as long as the logic doesn't change anything too fast.
		 if( updateTime >= UPDATE_RATE )
		 {
				updateTime = 0;
				state->Update();


			 window.clear();

			 state->Draw();

	         window.display();

			 timer.restart();
			// Reset the accumulator and the clock
		 }
     }
}
