#pragma once


//#include <SFML/System.hpp>
#include <SFML/Window.hpp>
//#include <SFML/Graphics.hpp>
#include "Game.h"


class IState
{
public:
	virtual ~IState() { game = NULL;	}
	int GetID() { return id; }
	virtual void DoInit() = 0;
	virtual void ReInit() = 0;
	void DeInit(){}
	bool IsPaused() { return paused; }
	virtual void Pause() { paused = true; }
	virtual void Resume() { paused = false; }
	virtual void HandleEvents(sf::Event event) = 0;
	virtual void Update(void) = 0;
	virtual void Draw() = 0;
	virtual void HandleInput() = 0;
	void HandleCleanup() { Cleanup(); }

private:
	int id;
	bool paused;

protected:
	Game* game;

	IState(Game* g) { game = g; paused = false;}
	virtual void Cleanup(){}

	/**
     * Our copy constructor is private because we do not allow copies of
     * our Singleton class
     */
    IState(const IState&);  // Intentionally undefined
 
    /**
     * Our assignment operator is private because we do not allow copies
     * of our Singleton class
     */
    IState& operator=(const IState&); // Intentionally undefined
};