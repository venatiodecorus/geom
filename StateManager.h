#pragma once

//#include "IState.h"
#include "types.h"
#include <vector>

class StateManager
{
public:
	StateManager(void);
	~StateManager(void);

	bool IsEmpty(void);
	void AddState(IState* state);
	IState* GetActiveState(void);
	void DropState(void);	
	void HandleCleanup(void);

private:
	std::vector<IState*> stack;
	IState* currentState;
	/**
     * StateManager copy constructor is private because we do not allow copies
     * of our class
     */
    StateManager(const StateManager&); // Intentionally undefined
 
    /**
     * Our assignment operator is private because we do not allow copies
     * of our class
     */
    StateManager& operator=(const StateManager&); // Intentionally undefined
};

