#include "ObjectManager.h"

ObjectsMap ObjectManager::objectsMap;
ObjectsMap::iterator ObjectManager::iter;

/**
 * Destructor, frees all allocated memory
 */
ObjectManager::~ObjectManager()
{
    for (iter = objectsMap.begin();
         iter != objectsMap.end();
         iter++)
    {
        delete iter->second;
    }
}

/**
 * Inserts an game's object pointer in the container.
 * 
 * @param object Object pointer to be inserted in the container
 * @return true if insert was successful, false if not
 */
bool ObjectManager::Insert(BaseObject* object)
{
    bool retVal = true;

    iter = objectsMap.find(object->GetId());

    if (iter == objectsMap.end())
    {
    	objectsMap[object->GetId()] = object;
    }
    else
    {
        retVal = false;
    }
    
    return retVal;
}

/**
 * Gets an object based on a key
 * 
 * @param key The key of the container pair
 * @return A pointer to the game's object
 */
BaseObject* ObjectManager::GetObject(unsigned int key)
{    
    return objectsMap[key];
}

/**
 * A function that emulates a for each loop. A function pointer of a function
 * that matches this pattern "void* Func(BaseObject* baseObject, void* params)"
 * must be passed and will be called for every element of the container.
 * Additional parameters can also be passed, depending on the use.
 *
 * @param ForEachFunc The pointer function to be called for every element
 * @param params Optional parameters that the calling application might need
 */
void ObjectManager::ForEach(ForEachFunc func, void* params)
{
    for (iter = objectsMap.begin();
         iter != objectsMap.end();
         iter++)
    {
        (*func)(iter->second, params);
    }
}