
#include <algorithm>
#include "Util.h"

unsigned int Util::id = 0;

/**
 * Checks if a value exceeds minimum and maximum values, and if it does it 
 * sets it to the corresponding limit
 * 
 * @param currentValue The current value to be checked for exceeding limits
 * @param minValue Minimum allowed value
 * @param maxValue Maximum allowed value
 * @return The corrected value
 */
float Util::Clamp (float currentValue, float minValue, float maxValue)
{
    return std::min(maxValue, std::max(currentValue, minValue));
}

/**
 * Gets the next id the game might require for any of its operations
 * @return 
 */
unsigned int Util::GetNextId ()
{
    return id++;
}
