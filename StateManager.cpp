#include "StateManager.h"
#include "IState.h"
#include <assert.h>


StateManager::StateManager(void)
{
}


StateManager::~StateManager(void)
{
	while(!stack.empty())
	{
		IState* s = stack.back();
		stack.pop_back();
		s->Pause();
		s->DeInit();
		s->HandleCleanup();
		delete s;
		s = NULL;
	}
}

bool StateManager::IsEmpty()
{
	return stack.empty();
}

void StateManager::AddState(IState* s)
{
	assert(s != NULL);

	if( !stack.empty() )
	{
		stack.back()->Pause();
	}

	stack.push_back(s);
	s->DoInit();
	currentState = s;
}

IState* StateManager::GetActiveState()
{
	return currentState;
}

void StateManager::DropState()
{
	if(stack.empty())
		return;

	IState* s = stack.back();
	s->Pause();
	s->DeInit();
	stack.pop_back();
	delete s;
	s = NULL;

	if( !stack.empty() )
	{
		stack.back()->Resume();
		currentState = stack.back();
	}
}