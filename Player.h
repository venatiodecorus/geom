#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "BaseObject.h"
#include "Level.h"
#include "Game.h"
#include <SFML/Graphics.hpp>

class Player : public BaseObject
{
public:
    Player(Game* game_,
           Level* level_,
           sf::Vector2f* pos_,
           sf::Vector2f* vel_);
    ~Player(void);

    void Update();

    const static unsigned int POLARITY_TIMEOUT; // Duration of inverted polarity
    const static int SHOOT_DELAY; // Unsigned was causing an error in PlayState.cpp
    int lives; // Number of lives the player has left
    
private:
    sf::Texture* texInverted;        
    sf::Clock polarityTimer; // Will keep track of polarity time
    sf::Clock shootTimer; // Will keep track of polarity time    
    Game* game;    
    Level* level;
    std::string path;  
    bool rClickDown; // Flag showing if right click is down    
    bool polarity;
    
    // Switches the player's polarity
    void Invert();
};

#endif /* _PLAYER_H_ */
