#include "SettingsReader.h"
#include <fstream>

/**
 * Reads the configuration file, calling parsing function for each line.
 * 
 * @param fileName The filename of the configuration file
 * @param settings Settings object to store the settings
 * @return true for success, false for fail
 */
bool SettingsReader::Read(const char* fileName, Settings& settings)
{
    bool retVal = true;
    std::ifstream cfg(fileName);
        
    if (!cfg)
    {
        retVal = false;
    }
    else
    {
        std::string line;
        std::string name;
        std::string value;
        int pos;
        
        while (std::getline(cfg, line))
        {
            if (!line.length())
            {
                continue;
            }

            pos = line.find('=');
            name = (line.substr(0, pos));
            value = trim(line.substr(pos+1));

            Read(settings, name, value);
        }
    }
    return retVal;
}

/**
 * Parses the line and if it finds a known name, it parses 
 * 
 * @param settings Settings object to store the settings
 * @param name Name of the setting
 * @param value Value of the setting
 */
void SettingsReader::Read(Settings& settings,
                          const std::string& name,
                          const std::string& value)
{
    ParseResolution(settings, name, value);
    ParseVSync(settings, name, value);
    ParseTitle(settings, name, value);
    ParseFrameLimit(settings, name, value);
}

/**
 * Trims the beginning and the end of a string, removing certain characters
 * 
 * @param source String to trim
 * @param delims Characters to check for trimming
 * @return Trimmed string
 */
std::string SettingsReader::trim(std::string const& source,
                                 char const* delims) {
    std::string result(source);
    std::string::size_type index = result.find_last_not_of(delims);
    if(index != std::string::npos)
    {
        result.erase(++index);
    }

    index = result.find_first_not_of(delims);
    if(index != std::string::npos)
    {
        result.erase(0, index);
    }
    else
    {
        result.erase();
    }
    return result;
}

void SettingsReader::ParseResolution(Settings& settings,
                                     const std::string& name,
                                     const std::string& value)
{
    if (name == "resolution")
    {
        int pos = value.find('x');
        std::string x = (value.substr(0, pos));
        std::string y = trim(value.substr(pos+1));
        settings.resolution.x = atoi(x.c_str());
        settings.resolution.y = atoi(y.c_str());
    }
}

void SettingsReader::ParseVSync(Settings& settings,
                                const std::string& name,
                                const std::string& value)
{
    if (name == "vsync")
    {
        if (value.compare("1") == 0)
        {
            settings.vsyc = true;
        }
        else if (value.compare("0") == 0)
        {
            settings.vsyc = false;
        }
    }
}

void SettingsReader::ParseTitle(Settings& settings,
                                const std::string& name,
                                const std::string& value)
{
    if (name == "title")
    {
        settings.title = value;
    }
}

void SettingsReader::ParseFrameLimit(Settings& settings,
                                     const std::string& name,
                                     const std::string& value)
{
    if (name == "frame_limit")
    {
        settings.frameLimit = atoi(value.c_str());
    }
}
