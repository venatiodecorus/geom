#include "AssetManager.h"

#include <iostream>

SpriteMap AssetManager::spriteMap;
SpriteMap AssetManageriter;

AssetManager::AssetManager()
{
}

AssetManager::~AssetManager()
{
}

void AssetManager::Initialise()
{
    cout << "Loading textures..." << endl;
    
    for (int i = FIRST+1; i != LAST; i++)
    {
        switch (i)
        {
            case PLAYER:
                spriteMap[PLAYER] = InitSprite("GA.png");
            break;
            case ENEMY:
                spriteMap[ENEMY] = InitSprite("enemy.png");
            break;
            case BULLET:
                spriteMap[BULLET] = InitSprite("bullet.png");
            break;
        }
    }
}

Texture* AssetManager::InitSprite(string path)
{
    Texture* texture = new Texture();
    if (texture->loadFromFile("graphics/" + path))
    {
        cout << "Texture \"" << path << "\" was loaded successfully." << endl;
    }
    else
    {
        cout << "Error while loading texture\"" << path << "\"." << endl;
    }
    
    return texture;
}

/**
 * Get the sprite for a specific texture.
 * 
 * @param spriteAsset Sprite asset to be fetched
 * @return            The textures
 * 
 * @NOTE: This function allocates memory for the texture, it is the calling 
 *         application's obligation to delete it.
 */
Sprite* AssetManager::GetSprite(SpriteAsset spriteAsset)
{
    Texture* texture = spriteMap[spriteAsset];

    Sprite* sprite = new Sprite();
    sprite->setTexture(*texture, true);
    sprite->setOrigin(texture->getSize().x/2, texture->getSize().y/2);
    
    return sprite;
}

Texture* AssetManager::GetTexture(SpriteAsset spriteAsset)
{
    return spriteMap[spriteAsset];
}
