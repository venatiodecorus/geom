#ifndef _COLLISION_H
#define	_COLLISION_H

#ifndef PI
    #define PI (3.14159265358979323846)
#endif
#define RADIANS_PER_DEGREE (PI/180.0)

class Collision
{
public:

    Collision() {}
    virtual ~Collision() {}

    /**
     *  Test for bounding box collision using OBB Box.
     *  To test against AABB use PixelPerfectTest with AlphaLimit = 0
     *
     *  @see Collision::PixelPerfectTest
     */
    static bool BoundingBoxTest(const sf::Sprite& Object1, const sf::Sprite& Object2);
    
    /**
     *  Helper function in order to rotate a point by an Angle
     *
     *  Rotation is CounterClockwise in order to match SFML Sprite Rotation
     *
     *  @param Point a Vector2f representing a coordinate
     *  @param Angle angle in degrees
     */
    static sf::Vector2f RotatePoint(const sf::Vector2f& Point, float Angle);
};

#endif	/* _COLLISION_H */
