/* 
 * File:   MenuState.cpp
 * Author: manji
 * 
 * Created on 22 April 2012, 18:11
 */

#include "MenuState.h"

MenuState::MenuState(Game* game) :
    IState(game),
    window(game->window)
{
    // Create the label.
    m_label = sfg::Label::Create( "Hello world!" );
 
    // Create a simple button and connect the click signal.
    sfg::Button::Ptr button(sfg::Button::Create("Greet SFGUI!"));
    button->OnLeftClick.Connect(&OnButtonClick,this );
 
    // Create a vertical box layout with 5 pixels spacing and add the label
    // and button to it.
    sfg::Box::Ptr box(sfg::Box::Create(sfg::Box::VERTICAL, 5.0f));
    box->Pack(m_label);
    box->Pack(button, false);
 
    // Create a window and add the box layouter to it. Also set the window's title.
    sfg::Window::Ptr window(sfg::Window::Create());
    window->SetTitle("Hello world!");
    window->Add(box);    
 
    // Create a desktop and add the window to it.    
    desktop.Add( window );
 
    // We're not using SFML to render anything in this program, so reset OpenGL
    // states. Otherwise we wouldn't see anything.
    game->window->resetGLStates(); 
}

MenuState::~MenuState() {
    
}

void MenuState::OnButtonClick() {
    m_label->SetText( "Hello SFGUI, pleased to meet you!" );
}

virtual void MenuState::Draw()
{
    while(window->isOpen())
    {
        // Event processing.
        while(window->pollEvent(event))
        {
            desktop.HandleEvent(event);
 
            // If window is about to be closed, leave program.
            if( event.type == sf::Event::Closed )
            {
                window->close();
            }
        }
 
        // Update SFGUI with elapsed seconds since last call.
        desktop.Update( clock.restart().asSeconds() );
 
        // Rendering
        window->clear();
        m_sfgui.Display(window);
        window->display();
    }
}