/* 
 * File:   MenuState.h
 * Author: manji
 *
 * Created on 22 April 2012, 18:11
 */

#include "IState.h"
#include <SFML/Graphics.hpp>
#include <SFGUI/SFGUI.hpp>

#ifndef MENUSTATE_H
#define	MENUSTATE_H

class MenuState : public IState
{
public:
    MenuState(Game* game);
    virtual ~MenuState();
    
	virtual void DoInit(){}
	virtual void ReInit(){}
	// Input will be checked in Update so I don't think HandleEvents needs to do anything
	virtual void HandleEvents(sf::Event event){}
	// Updates all the components of the state and handles logic
	virtual void Update(void){}
	// Checks for input and responds
	virtual void HandleInput(void){}
	// Draws everything in this state
	virtual void Draw(){}

    void OnButtonClick();
    
protected:
//	virtual void Cleanup();
    
private:
    // Create an SFGUI. This is required before doing anything with SFGUI.
    sfg::SFGUI m_sfgui;

    // Create the label pointer here to reach it from OnButtonClick().
    sfg::Label::Ptr m_label;                
    sfg::Desktop desktop;
    sf::RenderWindow window;
        // Main loop!
    sf::Event event;
    sf::Clock clock;

};

#endif	/* MENUSTATE_H */

