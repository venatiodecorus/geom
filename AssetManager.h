#ifndef _ASSET_MANAGER_H_
#define _ASSET_MANAGER_H_

#include <SFML/Graphics.hpp>
#include <unordered_map>
#include <string>

using namespace std;
using namespace sf;

enum SpriteAsset {
    FIRST,
    
    PLAYER,
    ENEMY,
    BULLET,
    
    LAST
};

typedef unordered_map<SpriteAsset, Texture*, hash<int> > SpriteMap;

class AssetManager
{
public:
    AssetManager();
    ~AssetManager();
    
    void Initialise();
    static Sprite* GetSprite(SpriteAsset);
    static Texture* GetTexture(SpriteAsset);
    
private:
    Texture* InitSprite(string path);
    
    static SpriteMap spriteMap;
    static SpriteMap::iterator iter;
};

#endif /* _PLAY_STATE_H_ */
