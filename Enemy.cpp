#include "Enemy.h"
#include "AssetManager.h"
#include <iostream>

Enemy::Enemy(sf::Vector2f* pos_, sf::Vector2f* vel_, Level* level_) :
    BaseObject(pos_, vel_, SpriteAsset::ENEMY), level(level_)
{
}

void Enemy::Update()
{
    if(pos->x + texture->getSize().x > level->MAX_WIDTH)
    {
        vel->x = -vel->x;
        pos->x = level->MAX_WIDTH - texture->getSize().x;
    }
    if(pos->x < 0)
    {
        vel->x = -vel->x;
        pos->x = 0;
    }
    if(pos->y + texture->getSize().y > level->MAX_HEIGHT)
    {
        vel->y = -vel->y;
        pos->y = level->MAX_HEIGHT - texture->getSize().y;
    }
    if(pos->y < 0)
    {
        vel->y = -vel->y;
        pos->y = 0;
    }
    
    BaseObject::Update();
}
