#include "types.h"
#include "IState.h"

class TitleState : public IState
{
public:
	TitleState(Game* game);
	~TitleState(void);

	virtual void DoInit();
	virtual void ReInit();
	virtual void HandleEvents(sf::Event event);
	virtual void Update(void);
	virtual void HandleInput(void);
	virtual void Draw();

protected:
	virtual void Cleanup();
};