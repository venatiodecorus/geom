#ifndef _OBJECT_MANAGER_H_
#define	_OBJECT_MANAGER_H_

#include "BaseObject.h"
#include <unordered_map>

class BaseObject;

typedef std::unordered_map<unsigned int, BaseObject*> ObjectsMap;
typedef void (*ForEachFunc)(BaseObject* baseObject, void* params);

class ObjectManager {
public:
    ObjectManager() {}
    ~ObjectManager();

    bool Insert(BaseObject*);
    BaseObject* GetObject(unsigned int key);
    static void ForEach(ForEachFunc func, void* params = NULL);

private:
    static ObjectsMap objectsMap;
    static ObjectsMap::iterator iter;
};

#endif	/* _OBJECT_MANAGER_H_ */