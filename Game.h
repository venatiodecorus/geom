#pragma once

#include "StateManager.h"
#include "Bullet.h"
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>


// This class acts as our app class, manages the game loop and exits when we're done
class Game
{
public:
	sf::RenderWindow window;
	sf::Clock timer;

	Game(void);
	~Game(void);

	void Init();
	void Run();   
    
private:
	StateManager stateMan;
	int UPDATE_RATE;
	int updateTime;
};

